from flask import Flask, request, session, g, url_for, abort, render_template,make_response,redirect
import flask_login
import hashlib
from mysql_db import MySQL
from Cryptodome.Cipher import AES
from Cryptodome import Random
import base64
import re

app = Flask(__name__)
app.debug = 1
app.secret_key = 'tik693jv26t9bj4r6egfbik3u3t2ufi49' 
login_manager = flask_login.LoginManager()
login_manager.init_app(app)
app.config.from_pyfile('config.py')
mysql = MySQL(app)
application = app
BS = 16
pad = lambda s: bytes(s + (BS - len(s) % BS) * chr(BS - len(s) % BS), 'utf-8')
unpad = lambda s : s[0:-ord(s[-1:])]
check_number_of_contract = False
check_document_series_and_number = False
check_student_ticket_registration_number = False
check_contract_number_for_the_provision_of_paid_services = False
check_date_of_issue = False
check_information_about_the_experience = False
check_name = False
check_surname = False
check_patronymic = False
check_date_of_conclusion_of_the_contract_for_the_provision_of_paid_ser = False

class AESCipher:

	def __init__( self, key ):
		self.key = bytes(key, 'utf-8')

	def encrypt( self, raw ):
		raw = pad(raw)
		iv = Random.new().read( AES.block_size )
		cipher = AES.new(self.key, AES.MODE_CFB, iv )
		return base64.b64encode( iv + cipher.encrypt( raw ) )

	def decrypt( self, enc ):
		enc = base64.b64decode(enc)
		iv = enc[:16]
		cipher = AES.new(self.key, AES.MODE_CFB, iv )
		return unpad(cipher.decrypt( enc[16:] )).decode('utf-8')

class User(flask_login.UserMixin):
	pass

@login_manager.user_loader
def user_loader(login):
	cursor = mysql.connection.cursor(named_tuple=True)
	cursor.execute('select login, password, name from e0bhkd9_Users where login=%s', (login,)) 
	user_from_db = cursor.fetchone()
	if user_from_db:
		user = User()
		user.id = user_from_db.login
		user.login = user_from_db.login
		user.password = user_from_db.password
		user.name = user_from_db.name
		return user
	return None 


@app.route('/', methods=['GET', 'POST'])
def login():
	try:
		if request.method == 'GET':
			return render_template('index.html')

		login = request.form.get('login')
		password = request.form.get('password')
		sha_login = hashlib.sha1(str(login).encode('utf-8')).hexdigest()
		sha_password = hashlib.sha1(str(password).encode('utf-8')).hexdigest()
		if login and password:
			cursor = mysql.connection.cursor(named_tuple=True)
			cursor.execute('select login, password, name from e0bhkd9_Users where login=%s and password=%s', (sha_login, sha_password)) 
			user_from_db = cursor.fetchone()
			if user_from_db:
				user = User()
				user.id = user_from_db.login
				user.login = user_from_db.login
				user.password = user_from_db.password
				user.name = user_from_db.name
				flask_login.login_user(user, remember = True)
				return redirect(url_for('personal_area'))
			else:
				msg = 'введён неверный логин или пароль'
		msg = 'введён неверный логин или пароль'
		return render_template('index.html', msg = msg,)
	except mysql.connector.errors.DatabaseError:
		msg = 'подключитесь к VPN серверу fit.mospolytech.ru'
		return render_template('index.html', msg = msg,)

@app.route('/logout')
def logout():
	try:
		flask_login.logout_user()
		return redirect(url_for('Main_paper'))
	except mysql.connector.errors.DatabaseError:
		msg = 'подключитесь к VPN серверу fit.mospolytech.ru'
		return render_template('index.html', msg = msg,)

@app.route('/')
def Main_paper():
    return render_template('index.html',)

@app.route('/services')
def services():
    return render_template('services.html',)

@app.route('/about-company')
def about_company():
	return render_template('about-company.html',)

@app.route('/personal-area')
def personal_area():
	login_sha1 = flask_login.current_user.login
	return render_template('personal-area.html', login_sha1 = login_sha1)

def check_auth():
	if flask_login.current_user.is_authenticated and flask_login.current_user.login == '1f5824f08c1b09cf6511dee2352e6f1681f94893':
		return True

@app.route("/personal-area/insert", methods=['POST'])
@flask_login.login_required
def personal_area_insert():
	if check_auth():
		try:
			cursor = mysql.connection.cursor(named_tuple=True)
			name = request.form.get('user_name')
			login = request.form.get('login')
			password = request.form.get('password')
			sha_new_lodin = hashlib.sha1(str(login).encode('utf-8')).hexdigest()
			sha_new_password = hashlib.sha1(str(password).encode('utf-8')).hexdigest()
			if name and sha_new_lodin and sha_new_password:
				cursor.execute('insert into e0bhkd9_Users(name, login, password) values (%s, %s, %s)', (name, sha_new_lodin, sha_new_password))
				mysql.connection.commit()
				return redirect(url_for('personal_area'))
			msg_insert = "вы не ввели логин и/или пароль"
			login_sha1 = flask_login.current_user.login
			return render_template('personal-area.html', msg_insert = msg_insert, login_sha1 = login_sha1)
		except mysql.connector.errors.DatabaseError:
			msg_insert = "вы не ввели логин и/или пароль"
			login_sha1 = flask_login.current_user.login
			return render_template('personal-area.html', msg_insert = msg_insert, login_sha1 = login_sha1)

@app.route("/personal-area/logpass", methods=['POST'])
@flask_login.login_required
def personal_area_logpass():
	login = request.form.get('old-login')
	password = request.form.get('old-password')
	sha_login = hashlib.sha1(str(login).encode('utf-8')).hexdigest()
	sha_password = hashlib.sha1(str(password).encode('utf-8')).hexdigest()
	if sha_login == flask_login.current_user.login and sha_password == flask_login.current_user.password:
		cursor = mysql.connection.cursor(named_tuple=True)
		cursor.execute('select id from e0bhkd9_Users where login=%s and password=%s', (sha_login, sha_password)) 
		check_logpass = cursor.fetchone()
		try:
			str_check_logpass = ' '.join([str(x) for x in check_logpass])
			if check_logpass:
				new_login = request.form.get('new-login')
				new_password = request.form.get('new-password')
				new_sha_login = hashlib.sha1(str(new_login).encode('utf-8')).hexdigest()
				new_sha_password = hashlib.sha1(str(new_password).encode('utf-8')).hexdigest()
				cursor = mysql.connection.cursor(named_tuple=True)
				cursor.execute('UPDATE `e0bhkd9_Users` SET `login`=%s,`password`=%s WHERE `id`=%s', (new_sha_login, new_sha_password, str_check_logpass))
				mysql.connection.commit()
				return redirect(url_for('Main_paper'))
		except TypeError :
			msg = "Вы ввели неверный действующий логин или пароль"
			login_sha1 = flask_login.current_user.login
	msg = "Вы ввели неверный действующий логин или пароль"
	login_sha1 = flask_login.current_user.login
	return render_template('personal-area.html', msg = msg, login_sha1 = login_sha1)

@app.route("/personal-area/delete", methods=['POST'])
@flask_login.login_required
def personal_area_delete():
	if check_auth():
		cursor = mysql.connection.cursor(named_tuple=True)
		user_id = request.form.get('user_id')
		if user_id:
			cursor.execute('select login from e0bhkd9_Users where id=%s', (user_id,)) 
			check_user = cursor.fetchone()
			if check_user != None:
				cursor = mysql.connection.cursor(named_tuple=True)
				cursor.execute('delete from e0bhkd9_Users where id=%s', (user_id,))
				mysql.connection.commit()
				return redirect(url_for('personal_area'))
			msg_del = "вы ввели неверный ID пользователя"
			login_sha1 = flask_login.current_user.login
			return render_template('personal-area.html', msg_del = msg_del, login_sha1 = login_sha1)
		msg_del = "вы ввели неверный ID пользователя"
		login_sha1 = flask_login.current_user.login
		return render_template('personal-area.html', msg_del = msg_del, login_sha1 = login_sha1)



@app.route("/personal-area/new-subject", methods=['POST'])
@flask_login.login_required
def personal_area_new_subject():
	name = request.form.get('name')
	surname = request.form.get('surname')
	patronymic = request.form.get('patronymic')
	number_of_contract = request.form.get('number_of_contract')
	the_level_of_education = request.form.get('the_level_of_education')
	type_of_document = request.form.get('type_of_document')
	document_series_and_number = request.form.get('document_series_and_number')
	date_of_issue = request.form.get('date_of_issue')
	who_issued_the_document = request.form.get('who_issued_the_document')
	type_and_name_of_educational_institution = request.form.get('type_and_name_of_educational_institution')
	training_direction_or_specialty = request.form.get('training_direction_or_specialty')
	document_qualification = request.form.get('document_qualification')
	information_about_military_registration = request.form.get('information_about_military_registration')
	student_class = request.form.get('class')
	student_ticket_registration_number = request.form.get('student_ticket_registration_number')
	place_of_work = request.form.get('place_of_work')
	position = request.form.get('position')
	type_of_work_document = request.form.get('type_of_work_document')
	information_about_the_experience = request.form.get('information_about_the_experience')
	contract_number_for_the_provision_of_paid_services = request.form.get('contract_number_for_the_provision_of_paid_services')
	date_of_conclusion_of_the_contract_for_the_provision_of_paid_ser = request.form.get('date_of_conclusion_of_the_contract_for_the_provision_of_paid_ser')
	if name and surname and patronymic and number_of_contract and the_level_of_education and type_of_document and document_series_and_number and date_of_issue and who_issued_the_document and type_and_name_of_educational_institution and training_direction_or_specialty and document_qualification and information_about_military_registration and student_class and student_ticket_registration_number and place_of_work and position and type_of_work_document and information_about_the_experience and contract_number_for_the_provision_of_paid_services and date_of_conclusion_of_the_contract_for_the_provision_of_paid_ser:
		key = 'Fm8St9TgUG77Bq52'
		cipher = AESCipher(key)
		encrypted_name = cipher.encrypt(name)
		encrypted_surname = cipher.encrypt(surname)
		encrypted_patronymic = cipher.encrypt(patronymic)
		encrypted_number_of_contract = cipher.encrypt(number_of_contract)
		encrypted_the_level_of_education = cipher.encrypt(the_level_of_education)
		encrypted_type_of_document = cipher.encrypt(type_of_document)
		encrypted_document_series_and_number = cipher.encrypt(document_series_and_number)
		encrypted_date_of_issue = cipher.encrypt(date_of_issue)
		encrypted_who_issued_the_document = cipher.encrypt(who_issued_the_document)
		encrypted_type_and_name_of_educational_institution = cipher.encrypt(type_and_name_of_educational_institution)
		encrypted_training_direction_or_specialty = cipher.encrypt(training_direction_or_specialty)
		encrypted_document_qualification = cipher.encrypt(document_qualification)
		encrypted_information_about_military_registration = cipher.encrypt(information_about_military_registration)
		encrypted_student_class = cipher.encrypt(student_class)
		encrypted_student_ticket_registration_number = cipher.encrypt(student_ticket_registration_number)
		encrypted_place_of_work = cipher.encrypt(place_of_work)
		encrypted_position = cipher.encrypt(position)
		encrypted_type_of_work_document = cipher.encrypt(type_of_work_document)
		encrypted_information_about_the_experience = cipher.encrypt(information_about_the_experience)
		encrypted_contract_number_for_the_provision_of_paid_services = cipher.encrypt(contract_number_for_the_provision_of_paid_services)
		encrypted_date_of_conclusion_of_the_contract_for_the_provision_of_paid_ser = cipher.encrypt(date_of_conclusion_of_the_contract_for_the_provision_of_paid_ser)
		if re.match(r'[А-Я][а-я]+', name) and len(name) != 1:
			check_name = True
		else:
			msg_name = 'не корректный ввод данных (пример: Иван)'
			login_sha1 = flask_login.current_user.login
			return render_template('personal-area.html', msg_name = msg_name, login_sha1 = login_sha1, name = name,surname = surname, patronymic = patronymic, number_of_contract = number_of_contract, the_level_of_education = the_level_of_education, type_of_document = type_of_document, document_series_and_number = document_series_and_number, date_of_issue = date_of_issue, who_issued_the_document = who_issued_the_document, type_and_name_of_educational_institution = type_and_name_of_educational_institution, training_direction_or_specialty = training_direction_or_specialty, document_qualification = document_qualification, information_about_military_registration = information_about_military_registration, student_class = student_class, student_ticket_registration_number = student_ticket_registration_number, place_of_work = place_of_work, position = position, type_of_work_document = type_of_work_document, information_about_the_experience = information_about_the_experience, contract_number_for_the_provision_of_paid_services = contract_number_for_the_provision_of_paid_services, date_of_conclusion_of_the_contract_for_the_provision_of_paid_ser = date_of_conclusion_of_the_contract_for_the_provision_of_paid_ser)
		if re.match(r'[А-Я][а-я]+', surname) and len(surname) != 1:
			check_surname = True
		else:
			msg_surname = 'не корректный ввод данных (пример: Иванов)'
			login_sha1 = flask_login.current_user.login
			return render_template('personal-area.html', msg_surname = msg_surname, login_sha1 = login_sha1, name = name,surname = surname, patronymic = patronymic, number_of_contract = number_of_contract, the_level_of_education = the_level_of_education, type_of_document = type_of_document, document_series_and_number = document_series_and_number, date_of_issue = date_of_issue, who_issued_the_document = who_issued_the_document, type_and_name_of_educational_institution = type_and_name_of_educational_institution, training_direction_or_specialty = training_direction_or_specialty, document_qualification = document_qualification, information_about_military_registration = information_about_military_registration, student_class = student_class, student_ticket_registration_number = student_ticket_registration_number, place_of_work = place_of_work, position = position, type_of_work_document = type_of_work_document, information_about_the_experience = information_about_the_experience, contract_number_for_the_provision_of_paid_services = contract_number_for_the_provision_of_paid_services, date_of_conclusion_of_the_contract_for_the_provision_of_paid_ser = date_of_conclusion_of_the_contract_for_the_provision_of_paid_ser)
		if re.match(r'[А-Я][а-я]+', patronymic) and len(patronymic) != 1:
			check_patronymic = True
		else:
			msg_patronymic = 'не корректный ввод данных (пример: Иванович)'
			login_sha1 = flask_login.current_user.login
			return render_template('personal-area.html', msg_patronymic = msg_patronymic, login_sha1 = login_sha1, name = name,surname = surname, patronymic = patronymic, number_of_contract = number_of_contract, the_level_of_education = the_level_of_education, type_of_document = type_of_document, document_series_and_number = document_series_and_number, date_of_issue = date_of_issue, who_issued_the_document = who_issued_the_document, type_and_name_of_educational_institution = type_and_name_of_educational_institution, training_direction_or_specialty = training_direction_or_specialty, document_qualification = document_qualification, information_about_military_registration = information_about_military_registration, student_class = student_class, student_ticket_registration_number = student_ticket_registration_number, place_of_work = place_of_work, position = position, type_of_work_document = type_of_work_document, information_about_the_experience = information_about_the_experience, contract_number_for_the_provision_of_paid_services = contract_number_for_the_provision_of_paid_services, date_of_conclusion_of_the_contract_for_the_provision_of_paid_ser = date_of_conclusion_of_the_contract_for_the_provision_of_paid_ser)
		if re.match(r'[0-9]{9}', number_of_contract) and len(number_of_contract) == 9:
			check_number_of_contract = True
		else:
			msg_number_of_contract = 'не корректный ввод данных (пример: 123456789)'
			login_sha1 = flask_login.current_user.login
			return render_template('personal-area.html', msg_number_of_contract = msg_number_of_contract, login_sha1 = login_sha1, name = name,surname = surname, patronymic = patronymic, number_of_contract = number_of_contract, the_level_of_education = the_level_of_education, type_of_document = type_of_document, document_series_and_number = document_series_and_number, date_of_issue = date_of_issue, who_issued_the_document = who_issued_the_document, type_and_name_of_educational_institution = type_and_name_of_educational_institution, training_direction_or_specialty = training_direction_or_specialty, document_qualification = document_qualification, information_about_military_registration = information_about_military_registration, student_class = student_class, student_ticket_registration_number = student_ticket_registration_number, place_of_work = place_of_work, position = position, type_of_work_document = type_of_work_document, information_about_the_experience = information_about_the_experience, contract_number_for_the_provision_of_paid_services = contract_number_for_the_provision_of_paid_services, date_of_conclusion_of_the_contract_for_the_provision_of_paid_ser = date_of_conclusion_of_the_contract_for_the_provision_of_paid_ser)
		if type_of_document == 'диплом':
			if re.match(r'[0-9]{12}', document_series_and_number) and len(document_series_and_number) == 12:
				check_document_series_and_number = True
			else:
				msg_document_series_and_number = 'не корректный ввод данных (пример: 123456789123)'
				login_sha1 = flask_login.current_user.login
				return render_template('personal-area.html', msg_document_series_and_number = msg_document_series_and_number, login_sha1 = login_sha1, name = name,surname = surname, patronymic = patronymic, number_of_contract = number_of_contract, the_level_of_education = the_level_of_education, type_of_document = type_of_document, document_series_and_number = document_series_and_number, date_of_issue = date_of_issue, who_issued_the_document = who_issued_the_document, type_and_name_of_educational_institution = type_and_name_of_educational_institution, training_direction_or_specialty = training_direction_or_specialty, document_qualification = document_qualification, information_about_military_registration = information_about_military_registration, student_class = student_class, student_ticket_registration_number = student_ticket_registration_number, place_of_work = place_of_work, position = position, type_of_work_document = type_of_work_document, information_about_the_experience = information_about_the_experience, contract_number_for_the_provision_of_paid_services = contract_number_for_the_provision_of_paid_services, date_of_conclusion_of_the_contract_for_the_provision_of_paid_ser = date_of_conclusion_of_the_contract_for_the_provision_of_paid_ser)
		elif type_of_document == 'аттестат':
			if re.match(r'[0-9]{14}', document_series_and_number) and len(document_series_and_number) == 14:
				check_document_series_and_number = True
			else:
				msg_document_series_and_number = 'не корректный ввод данных (пример: 12345678912345)'
				login_sha1 = flask_login.current_user.login
				return render_template('personal-area.html', msg_document_series_and_number = msg_document_series_and_number, login_sha1 = login_sha1, name = name,surname = surname, patronymic = patronymic, number_of_contract = number_of_contract, the_level_of_education = the_level_of_education, type_of_document = type_of_document, document_series_and_number = document_series_and_number, date_of_issue = date_of_issue, who_issued_the_document = who_issued_the_document, type_and_name_of_educational_institution = type_and_name_of_educational_institution, training_direction_or_specialty = training_direction_or_specialty, document_qualification = document_qualification, information_about_military_registration = information_about_military_registration, student_class = student_class, student_ticket_registration_number = student_ticket_registration_number, place_of_work = place_of_work, position = position, type_of_work_document = type_of_work_document, information_about_the_experience = information_about_the_experience, contract_number_for_the_provision_of_paid_services = contract_number_for_the_provision_of_paid_services, date_of_conclusion_of_the_contract_for_the_provision_of_paid_ser = date_of_conclusion_of_the_contract_for_the_provision_of_paid_ser)
		if re.match(r'[0-9]{2}[\.]{1}[0-9]{2}[\.]{1}[0-9]{4}', date_of_issue) and len(date_of_issue) == 10:
			check_date_of_issue = True
		else:
			msg_date_of_issue = 'не корректный ввод данных (пример: 01.01.2000)'
			login_sha1 = flask_login.current_user.login
			return render_template('personal-area.html', msg_date_of_issue = msg_date_of_issue, login_sha1 = login_sha1, name = name,surname = surname, patronymic = patronymic, number_of_contract = number_of_contract, the_level_of_education = the_level_of_education, type_of_document = type_of_document, document_series_and_number = document_series_and_number, date_of_issue = date_of_issue, who_issued_the_document = who_issued_the_document, type_and_name_of_educational_institution = type_and_name_of_educational_institution, training_direction_or_specialty = training_direction_or_specialty, document_qualification = document_qualification, information_about_military_registration = information_about_military_registration, student_class = student_class, student_ticket_registration_number = student_ticket_registration_number, place_of_work = place_of_work, position = position, type_of_work_document = type_of_work_document, information_about_the_experience = information_about_the_experience, contract_number_for_the_provision_of_paid_services = contract_number_for_the_provision_of_paid_services, date_of_conclusion_of_the_contract_for_the_provision_of_paid_ser = date_of_conclusion_of_the_contract_for_the_provision_of_paid_ser)
		if student_ticket_registration_number[0:2] == 'БС' or student_ticket_registration_number[0:2] == 'ПК' or student_ticket_registration_number[0:2] == 'АВ':
			if re.match(r'[БС|ПК|АВ]{2}[0-9]{1}[-]{1}[0-9]{2}[-]{1}[0-9]{3}', student_ticket_registration_number) and len(student_ticket_registration_number) == 10:
				check_student_ticket_registration_number = True
		else:
			msg_student_ticket_registration_number = 'не корректный ввод данных (пример: БС1-11-111)'
			login_sha1 = flask_login.current_user.login
			return render_template('personal-area.html', msg_student_ticket_registration_number = msg_student_ticket_registration_number, login_sha1 = login_sha1, name = name,surname = surname, patronymic = patronymic, number_of_contract = number_of_contract, the_level_of_education = the_level_of_education, type_of_document = type_of_document, document_series_and_number = document_series_and_number, date_of_issue = date_of_issue, who_issued_the_document = who_issued_the_document, type_and_name_of_educational_institution = type_and_name_of_educational_institution, training_direction_or_specialty = training_direction_or_specialty, document_qualification = document_qualification, information_about_military_registration = information_about_military_registration, student_class = student_class, student_ticket_registration_number = student_ticket_registration_number, place_of_work = place_of_work, position = position, type_of_work_document = type_of_work_document, information_about_the_experience = information_about_the_experience, contract_number_for_the_provision_of_paid_services = contract_number_for_the_provision_of_paid_services, date_of_conclusion_of_the_contract_for_the_provision_of_paid_ser = date_of_conclusion_of_the_contract_for_the_provision_of_paid_ser)
		if (re.match(r'[0-9]{2}', information_about_the_experience) or re.match(r'[0-9]{1}', information_about_the_experience)) and (len(information_about_the_experience) == 1 or len(information_about_the_experience) == 2):
			check_information_about_the_experience = True
		else:
			msg_information_about_the_experience = 'не корректный ввод данных (пример: 1) если субъект не имеет стажа, то впишите 0'
			login_sha1 = flask_login.current_user.login
			return render_template('personal-area.html', msg_information_about_the_experience = msg_information_about_the_experience, login_sha1 = login_sha1, name = name,surname = surname, patronymic = patronymic, number_of_contract = number_of_contract, the_level_of_education = the_level_of_education, type_of_document = type_of_document, document_series_and_number = document_series_and_number, date_of_issue = date_of_issue, who_issued_the_document = who_issued_the_document, type_and_name_of_educational_institution = type_and_name_of_educational_institution, training_direction_or_specialty = training_direction_or_specialty, document_qualification = document_qualification, information_about_military_registration = information_about_military_registration, student_class = student_class, student_ticket_registration_number = student_ticket_registration_number, place_of_work = place_of_work, position = position, type_of_work_document = type_of_work_document, information_about_the_experience = information_about_the_experience, contract_number_for_the_provision_of_paid_services = contract_number_for_the_provision_of_paid_services, date_of_conclusion_of_the_contract_for_the_provision_of_paid_ser = date_of_conclusion_of_the_contract_for_the_provision_of_paid_ser)
		if (re.match(r'[0-9]{9}', contract_number_for_the_provision_of_paid_services) or re.match(r'[-]{1}', contract_number_for_the_provision_of_paid_services)) and (len(contract_number_for_the_provision_of_paid_services) == 9 or len(contract_number_for_the_provision_of_paid_services) == 1):
			check_contract_number_for_the_provision_of_paid_services = True
		else:
			msg_contract_number_for_the_provision_of_paid_services = 'не корректный ввод данных (пример: 123456789)'
			login_sha1 = flask_login.current_user.login
			return render_template('personal-area.html', msg_contract_number_for_the_provision_of_paid_services = msg_contract_number_for_the_provision_of_paid_services, login_sha1 = login_sha1, name = name,surname = surname, patronymic = patronymic, number_of_contract = number_of_contract, the_level_of_education = the_level_of_education, type_of_document = type_of_document, document_series_and_number = document_series_and_number, date_of_issue = date_of_issue, who_issued_the_document = who_issued_the_document, type_and_name_of_educational_institution = type_and_name_of_educational_institution, training_direction_or_specialty = training_direction_or_specialty, document_qualification = document_qualification, information_about_military_registration = information_about_military_registration, student_class = student_class, student_ticket_registration_number = student_ticket_registration_number, place_of_work = place_of_work, position = position, type_of_work_document = type_of_work_document, information_about_the_experience = information_about_the_experience, contract_number_for_the_provision_of_paid_services = contract_number_for_the_provision_of_paid_services, date_of_conclusion_of_the_contract_for_the_provision_of_paid_ser = date_of_conclusion_of_the_contract_for_the_provision_of_paid_ser)
		if (re.match(r'[0-9]{2}[\.]{1}[0-9]{2}[\.]{1}[0-9]{4}', date_of_conclusion_of_the_contract_for_the_provision_of_paid_ser) or re.match(r'[-]{1}', date_of_conclusion_of_the_contract_for_the_provision_of_paid_ser)) and (len(date_of_conclusion_of_the_contract_for_the_provision_of_paid_ser) == 10 or len(date_of_conclusion_of_the_contract_for_the_provision_of_paid_ser) == 1):
			check_date_of_conclusion_of_the_contract_for_the_provision_of_paid_ser = True
		else:
			msg_date_of_conclusion_of_the_contract_for_the_provision_of_paid_ser = 'не корректный ввод данных (пример: 01.01.2000)'
			login_sha1 = flask_login.current_user.login
			return render_template('personal-area.html', msg_date_of_conclusion_of_the_contract_for_the_provision_of_paid_ser = msg_date_of_conclusion_of_the_contract_for_the_provision_of_paid_ser, login_sha1 = login_sha1, name = name,surname = surname, patronymic = patronymic, number_of_contract = number_of_contract, the_level_of_education = the_level_of_education, type_of_document = type_of_document, document_series_and_number = document_series_and_number, date_of_issue = date_of_issue, who_issued_the_document = who_issued_the_document, type_and_name_of_educational_institution = type_and_name_of_educational_institution, training_direction_or_specialty = training_direction_or_specialty, document_qualification = document_qualification, information_about_military_registration = information_about_military_registration, student_class = student_class, student_ticket_registration_number = student_ticket_registration_number, place_of_work = place_of_work, position = position, type_of_work_document = type_of_work_document, information_about_the_experience = information_about_the_experience, contract_number_for_the_provision_of_paid_services = contract_number_for_the_provision_of_paid_services, date_of_conclusion_of_the_contract_for_the_provision_of_paid_ser = date_of_conclusion_of_the_contract_for_the_provision_of_paid_ser)
		if check_number_of_contract == True and check_document_series_and_number == True and check_student_ticket_registration_number == True and check_contract_number_for_the_provision_of_paid_services == True and check_date_of_issue == True and check_information_about_the_experience == True and check_name == True and check_surname == True and check_patronymic == True and check_date_of_conclusion_of_the_contract_for_the_provision_of_paid_ser == True:
			cursor = mysql.connection.cursor(named_tuple=True)
			cursor.execute('insert into ib6beo2fv_students(name, surname, patronymic, number_of_contract, the_level_of_education, type_of_document, document_series_and_number, date_of_issue, who_issued_the_document, type_and_name_of_educational_institution, training_direction_or_specialty, document_qualification, information_about_military_registration, student_class, student_ticket_registration_number, place_of_work, position, type_of_work_document, information_about_the_experience, contract_number_for_the_provision_of_paid_services, date_of_conclusion_of_the_contract_for_the_provision_of_paid_ser) values (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)', (encrypted_name, encrypted_surname, encrypted_patronymic, encrypted_number_of_contract, encrypted_the_level_of_education, encrypted_type_of_document, encrypted_document_series_and_number, encrypted_date_of_issue, encrypted_who_issued_the_document, encrypted_type_and_name_of_educational_institution, encrypted_training_direction_or_specialty, encrypted_document_qualification, encrypted_information_about_military_registration, encrypted_student_class, encrypted_student_ticket_registration_number, encrypted_place_of_work, encrypted_position, encrypted_type_of_work_document, encrypted_information_about_the_experience, encrypted_contract_number_for_the_provision_of_paid_services, encrypted_date_of_conclusion_of_the_contract_for_the_provision_of_paid_ser))
			mysql.connection.commit()
			return redirect(url_for('personal_area'))
	msg_new_subject = 'не корректный ввод данных'
	login_sha1 = flask_login.current_user.login
	return render_template('personal-area.html', msg_new_subject = msg_new_subject, login_sha1 = login_sha1, name = name,surname = surname, patronymic = patronymic, number_of_contract = number_of_contract, the_level_of_education = the_level_of_education, type_of_document = type_of_document, document_series_and_number = document_series_and_number, date_of_issue = date_of_issue, who_issued_the_document = who_issued_the_document, type_and_name_of_educational_institution = type_and_name_of_educational_institution, training_direction_or_specialty = training_direction_or_specialty, document_qualification = document_qualification, information_about_military_registration = information_about_military_registration, student_class = student_class, student_ticket_registration_number = student_ticket_registration_number, place_of_work = place_of_work, position = position, type_of_work_document = type_of_work_document, information_about_the_experience = information_about_the_experience, contract_number_for_the_provision_of_paid_services = contract_number_for_the_provision_of_paid_services, date_of_conclusion_of_the_contract_for_the_provision_of_paid_ser = date_of_conclusion_of_the_contract_for_the_provision_of_paid_ser)

@app.route("/personal-area/info-subject", methods=['POST'])
@flask_login.login_required
def personal_area_info_subject():
	subject_ID = request.form.get('subject_ID')
	cursor = mysql.connection.cursor(named_tuple=True)
	cursor.execute('select name, surname, patronymic, number_of_contract, the_level_of_education, type_of_document, document_series_and_number, date_of_issue, who_issued_the_document, type_and_name_of_educational_institution, training_direction_or_specialty, document_qualification, information_about_military_registration,student_class, student_ticket_registration_number, place_of_work, position, type_of_work_document, information_about_the_experience, contract_number_for_the_provision_of_paid_services, date_of_conclusion_of_the_contract_for_the_provision_of_paid_ser from ib6beo2fv_students where id=%s', (subject_ID,))
	PD_subject = cursor.fetchone()
	encrypted_PD_subject = ''
	if PD_subject:
		for i in range(len(PD_subject)):
			str_PD_subject = ' '.join([str(x) for x in PD_subject[i]])
			key = 'Fm8St9TgUG77Bq52'
			cipher = AESCipher(key)
			encrypted_PD_subject += cipher.decrypt(str_PD_subject)
			encrypted_PD_subject += '|'
		result = re.split(r'[\|]', encrypted_PD_subject)
		login_sha1 = flask_login.current_user.login
		return render_template('personal-area.html', result = result, login_sha1 = login_sha1)
	msg_info_subject = 'введён не корректный ID'
	login_sha1 = flask_login.current_user.login
	result = None
	return render_template('personal-area.html',login_sha1 = login_sha1, msg_info_subject = msg_info_subject, result = result)

@app.route("/personal-area/delete-subject", methods=['POST'])
@flask_login.login_required
def personal_area_delete_subject():
	if check_auth():
		cursor = mysql.connection.cursor(named_tuple=True)
		subject_id = request.form.get('subject_id')
		if subject_id:
			cursor.execute('select name from ib6beo2fv_students where id=%s', (subject_id,)) 
			check_user = cursor.fetchone()
			if check_user != None:
				cursor = mysql.connection.cursor(named_tuple=True)
				cursor.execute('delete from ib6beo2fv_students where id=%s', (subject_id,))
				mysql.connection.commit()
				return redirect(url_for('personal_area'))
			msg_delete_subject = "вы ввели неверный ID субъекта"
			login_sha1 = flask_login.current_user.login
			return render_template('personal-area.html', msg_delete_subject = msg_delete_subject, login_sha1 = login_sha1)
		msg_delete_subject = "вы ввели неверный ID субъекта"
		login_sha1 = flask_login.current_user.login
		return render_template('personal-area.html', msg_delete_subject = msg_delete_subject, login_sha1 = login_sha1)

if __name__ == '__main__':
    app.run()